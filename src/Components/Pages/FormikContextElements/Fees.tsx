import React from "react";

import { Typography } from "@chainsafe/common-components";
import { useFormikContext } from "formik";

interface IFeesFormikWrapped {
  className?: string;
  symbol?: string;
  fee?: number;
  feeSymbol?: string;
  amountFormikName: string;
}

const FeesFormikWrapped: React.FC<IFeesFormikWrapped> = ({
  className,
  symbol,
  fee,
  feeSymbol,
  amountFormikName,
}: IFeesFormikWrapped) => {
  const { values } = useFormikContext();

  const inlineStyles = {
    primaryText: {
      fontFamily: "gilroy-medium",
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#1C1E24",
    },
    secondaryText: {
      fontFamily: "gilroy-medium",
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#5D616D",
    },
  };

  return (
    <section className={className}>
      {fee !== undefined && feeSymbol !== undefined && (
        <>
          <p style={inlineStyles.primaryText}>Bridge Fee</p>
          <p style={inlineStyles.secondaryText}>
            {fee} {feeSymbol}
          </p>
        </>
      )}
      {symbol !== undefined && (
        <>
          <p style={inlineStyles.primaryText}>Transfer Amount:</p>
          <p style={inlineStyles.secondaryText}>
            {Number((values as Record<string, any>)[amountFormikName])?.toFixed(3)} {symbol}
          </p>
        </>
      )}
    </section>
  );
};

export default FeesFormikWrapped;
