import React, { useEffect, useState } from "react";
import { makeStyles, createStyles, ITheme } from "@chainsafe/common-theme";
import AboutDrawer from "../../Modules/AboutDrawer";
import ChangeNetworkDrawer from "../../Modules/ChangeNetworkDrawer";
import PreflightModalTransfer from "../../Modules/PreflightModalTransfer";
import {
  Button,
  Typography,
  QuestionCircleSvg,
  SelectInput,
} from "@chainsafe/common-components";
import { Form, Formik, useField } from "formik";
import AddressInput from "../Custom/AddressInput";
import clsx from "clsx";
import TransferActiveModal from "../../Modules/TransferActiveModal";
import { useChainbridge } from "../../Contexts/ChainbridgeContext";
import TokenSelectInput from "../Custom/TokenSelectInput";
import TokenInput from "../Custom/TokenInput";
import { object, string } from "yup";
import { utils } from "ethers";
import FeesFormikWrapped from "./FormikContextElements/Fees";
import { useNetworkManager } from "../../Contexts/NetworkManagerContext";
import NetworkUnsupportedModal from "../../Modules/NetworkUnsupportedModal";
import { isValidSubstrateAddress } from "../../Utils/Helpers";
import { useHomeBridge } from "../../Contexts/HomeBridgeContext";
import { showImageUrl } from "../../Utils/Helpers";
import Balance from "../Custom/Balance";

const useStyles = makeStyles(({ constants, palette, breakpoints }: ITheme) =>
  createStyles({
    root: {
      padding: "35px",
      position: "relative",
      background: "#fff",
      borderRadius: 16,
      overflow: "hidden",
    },
    walletArea: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      width: "100%",
    },
    connectButton: {
      margin: `${constants.generalUnit * 3}px 0 ${constants.generalUnit * 6}px`,
    },
    connecting: {
      textAlign: "center",
      marginBottom: constants.generalUnit * 2,
    },
    connected: {
      width: "100%",
      "& > *:first-child": {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        width: "100%",
      },
    },
    changeButton: {
      cursor: "pointer",
    },
    networkName: {
      padding: `${constants.generalUnit * 2}px ${
        constants.generalUnit * 1.5
      }px`,
      border: `1px solid ${palette.additional["gray"][6]}`,
      borderRadius: 2,
      color: palette.additional["gray"][9],
      marginTop: constants.generalUnit,
      marginBottom: constants.generalUnit * 3,
    },
    formArea: {
      "&.disabled": {
        opacity: 0.5,
        pointerEvents: "none",
      },
    },
    currencySection: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "flex-start",
      margin: `${constants.generalUnit * 3}px 0`,
    },
    tokenInputArea: {
      display: "flex",
      flexDirection: "row",
      alignItems: "flex-end",
      justifyContent: "space-around",
      gridGap: 5,
    },
    tokenInputSection: {
      width: "50%",
      [breakpoints.down("sm")]: {
        width: "55%",
      },
    },
    tokenInput: {
      margin: 0,
      "& > span": {
        marginBottom: "14px !important",
      },
      "& > div": {
        height: 32,
        "& input": {
          background: "#E4EBFD !important",
          borderRadius: 8,
          padding: "10px 1em !important",
          fontFamily: "gilroy-medium",
          fontSize: "14px !important",
          lineHeight: "22px !important",
          border: "none !important",
          outline: "none !important",
        },
      },
      "& span:last-child.error": {
        position: "absolute",
        width: "calc(100% + 62px)",
        fontSize: 12,
        lineHeight: "18px",
        fontFamily: "gilroy-medium",
        marginTop: 8,
      },
    },
    maxButton: {
      fontFamily: "gilroy-bold",
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      background: "#8987DF",
      borderRadius: "8px",
      display: "grid",
      placeItems: "center",
      color: "#FFF",
      border: "1px solid #8987DF",
      outline: "none",
      padding: "9px 14px !important",
      transform: "translateY(4.5px)",
      "&:hover": {
        border: "1px solid #8987DF",
        backgroundColor: "#fff",
        color: "#8987DF",
      },
      "&:focus": {
        backgroundColor: "#fff",
        border: "1px solid #8987DF",
        color: "#8987DF",
      },
      "&:disabled": {
        opacity: "0.6",
        background: "#8987DF !important",
        color: "#FFF !important",
      },
    },
    currencySelector: {
      width: "50%",
      [breakpoints.down("sm")]: {
        width: "45%",
      },
      paddingRight: constants.generalUnit,
      "& *": {
        cursor: "pointer",
      },
    },
    token: {},
    address: {
      margin: 0,
      marginBottom: constants.generalUnit * 3,
    },
    addressInput: {},
    generalInput: {
      "& > span": {
        marginBottom: 10,
        fontFamily: "gilroy-bold",
        fontSize: "14px",
        lineHeight: "22px",
        letterSpacing: "0.03em",
        color: "#1C1E24",
      },
      "& >div": {
        borderRadius: 8,
        border: "none",
        "&:hover": {
          border: "none",
        },
      },
      "& >div>div": {
        background: "#E4EBFD",
        borderRadius: 8,
        padding: "7px 1em !important",
        fontFamily: "gilroy-medium",
        fontSize: "14px",
        lineHeight: "22px",
        border: "none",
        outline: "none",
        cursor: "pointer",
        [breakpoints.down("sm")]: {
          padding: "6px 1em !important",
        },
      },
      "&  [class$='indicatorSeparator']": {
        display: "none !important",
      },
      "& [class$='menu']": {
        "& >*": {
          background: "#E4EBFD",
        },
      },
    },
    faqButton: {
      cursor: "pointer",
      height: 20,
      width: 20,
      marginTop: constants.generalUnit * 5,
      fill: `${palette.additional["transferUi"][1]} !important`,
    },
    tokenItem: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      alignItems: "center",
      cursor: "pointer",
      "& img, & svg": {
        display: "block",
        height: 14,
        width: 14,
        marginRight: 10,
      },
      "& span": {
        minWidth: `calc(100% - 20px)`,
        textAlign: "left",
      },
    },
    fees: {
      display: "flex",
      flexDirection: "row",
      flexWrap: "wrap",
      justifyContent: "space-between",
      marginBottom: constants.generalUnit,
      "& > *": {
        display: "block",
        width: "50%",
        color: palette.additional["gray"][8],
        marginBottom: constants.generalUnit / 2,
        "&:nth-child(even)": {
          textAlign: "right",
        },
      },
    },
    accountSelector: {
      marginBottom: 24,
    },
    submitButton: {
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#FFFFFF",
      background: "#8269D8",
      borderRadius: "100px",
      padding: "6px 24px",
      outline: "none",
      border: "none",
      cursor: "pointer",
      width: "100%",
      fontFamily: "gilroy-medium",
      opacity: 1,
      "&:disabled": {
        cursor: "default",
        opacity: 0.5,
      },
      marginBottom: 25,
    },
    flex: {
      display: "flex",
      alignItems: "center",
      justifyContent: "space-between",
    },
    secondaryText: {
      fontFamily: "gilroy-bold",
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#1C1E24",
    },
    primaryText: {
      fontFamily: "gilroy-bold",
      fontSize: "16px",
      lineHeight: "24px",
      letterSpacing: "0.03em",
      color: "#8269D8",
    },
    container: {
      overflow: "auto",
      marginBottom: 25,
    },
    ternaryText: {
      fontFamily: "gilroy-medium",
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#5D616D",
    },
    tokenLabel: {
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#FFF !important",
      marginBottom: 12,
    },
  })
);

type PreflightDetails = {
  tokenAmount: number;
  token: string;
  tokenSymbol: string;
  receiver: string;
};

const TransferPage = () => {
  const classes = useStyles();
  const { walletType, setWalletType } = useNetworkManager();

  const {
    deposit,
    setDestinationChain,
    transactionStatus,
    resetDeposit,
    bridgeFee,
    tokens,
    isReady,
    homeConfig,
    destinationChainConfig,
    destinationChains,
    address,
    checkSupplies,
  } = useChainbridge();

  const { accounts, selectAccount } = useHomeBridge();
  const [aboutOpen, setAboutOpen] = useState<boolean>(false);
  const [walletConnecting, setWalletConnecting] = useState(false);
  const [changeNetworkOpen, setChangeNetworkOpen] = useState<boolean>(false);
  const [preflightModalOpen, setPreflightModalOpen] = useState<boolean>(false);

  const [preflightDetails, setPreflightDetails] = useState<PreflightDetails>({
    receiver: "",
    token: "",
    tokenAmount: 0,
    tokenSymbol: "",
  });

  useEffect(() => {
    if (walletType !== "select" && walletConnecting === true) {
      setWalletConnecting(false);
    } else if (walletType === "select") {
      setWalletConnecting(true);
    }
  }, [walletType, walletConnecting]);

  const selectedToken = homeConfig?.tokens.find(
    (token) => token.address === preflightDetails.token
  );

  const DECIMALS =
    selectedToken && selectedToken.decimals ? selectedToken.decimals : 18;

  const REGEX =
    DECIMALS > 0
      ? new RegExp(`^[0-9]{1,18}(.[0-9]{1,${DECIMALS}})?$`)
      : new RegExp(`^[0-9]{1,18}?$`);

  const transferSchema = object().shape({
    tokenAmount: string()
      .test("Token selected", "Please select a token", (value) => {
        if (
          !!value &&
          preflightDetails &&
          tokens[preflightDetails.token] &&
          tokens[preflightDetails.token].balance !== undefined
        ) {
          return true;
        } else {
          return false;
        }
      })
      .test("InputValid", "Input invalid", (value) => {
        try {
          return REGEX.test(`${value}`);
        } catch (error) {
          console.error(error);
          return false;
        }
      })
      .test("Max", "Insufficent funds", (value) => {
        if (
          value &&
          preflightDetails &&
          tokens[preflightDetails.token] &&
          tokens[preflightDetails.token].balance
        ) {
          if (homeConfig?.type === "Ethereum") {
            return parseFloat(value) <= tokens[preflightDetails.token].balance;
          } else {
            return (
              parseFloat(value + (bridgeFee || 0)) <=
              tokens[preflightDetails.token].balance
            );
          }
        }
        return false;
      })
      .test(
        "Bridge Supplies",
        "Not enough tokens on the destination chain. Please contact support.",
        async (value) => {
          if (checkSupplies && destinationChainConfig && value) {
            const supplies = await checkSupplies(
              parseFloat(value),
              preflightDetails.token,
              destinationChainConfig.chainId
            );
            return Boolean(supplies);
          }
          return false;
        }
      )
      .test("Min", "Less than minimum", (value) => {
        if (value) {
          return parseFloat(value) > 0;
        }
        return false;
      })
      .required("Please set a value"),
    token: string().required("Please select a token"),
    receiver: string()
      .test("Valid address", "Please add a valid address", (value) => {
        if (destinationChainConfig?.type === "Substrate") {
          return isValidSubstrateAddress(value as string);
        }
        return utils.isAddress(value as string);
      })
      .required("Please add a receiving address"),
  });

  return (
    <article className={classes.root}>
      {/* <div className={classes.container}>
        <div className={classes.flex}>
          <h4 className={classes.secondaryText}>Current balance</h4>
          <p className={classes.primaryText}>0 WEGEM</p>
        </div>
      </div> */}
      <Formik
        initialValues={{
          tokenAmount: 0,
          token: "",
          receiver: "",
        }}
        validateOnChange={false}
        validationSchema={transferSchema}
        onSubmit={(values) => {
          setPreflightDetails({
            ...values,
            tokenSymbol: tokens[values.token].symbol || "",
          });
          setPreflightModalOpen(true);
        }}
      >
        {(props) => (
          <Form
            className={clsx(classes.formArea, {
              disabled: !homeConfig || !address || props.isValidating,
            })}
          >
            <section>
              <SelectInput
                label="Destination Network"
                className={classes.generalInput}
                disabled={!homeConfig}
                options={destinationChains.map((dc) => ({
                  label: dc.name,
                  value: dc.chainId,
                }))}
                onChange={(value) => setDestinationChain(value)}
                value={destinationChainConfig?.chainId}
              />
            </section>
            <section className={classes.currencySection}>
              <section className={classes.currencySelector}>
                <TokenSelectInput
                  tokens={tokens}
                  name="token"
                  disabled={!destinationChainConfig}
                  // label={`"}
                  className={classes.generalInput}
                  placeholder=""
                  sync={(tokenAddress) => {
                    setPreflightDetails({
                      ...preflightDetails,
                      token: tokenAddress,
                      receiver: "",
                      tokenAmount: 0,
                      tokenSymbol: "",
                    });
                  }}
                  options={
                    Object.keys(tokens).map((t) => ({
                      value: t,
                      label: (
                        <div className={classes.tokenItem}>
                          {tokens[t]?.imageUri && (
                            <img
                              src={showImageUrl(tokens[t]?.imageUri)}
                              alt={tokens[t]?.symbol}
                            />
                          )}
                          <span>{tokens[t]?.symbol || t}</span>
                        </div>
                      ),
                    })) || []
                  }
                />
              </section>
              <section className={classes.tokenInputSection}>
                <div
                  className={clsx(classes.tokenInputArea, classes.generalInput)}
                >
                  <TokenInput
                    classNames={{
                      input: clsx(classes.tokenInput, classes.generalInput),
                      button: classes.maxButton,
                    }}
                    tokenSelectorKey="token"
                    tokens={tokens}
                    disabled={
                      !destinationChainConfig ||
                      !preflightDetails.token ||
                      preflightDetails.token === ""
                    }
                    name="tokenAmount"
                    label="."
                    labelClassName={classes.tokenLabel}
                  />
                </div>
              </section>
            </section>
            <section>
              <AddressInput
                disabled={!destinationChainConfig}
                name="receiver"
                label="Destination Address"
                placeholder="Enter the receiving address"
                className={classes.address}
                classNames={{
                  input: classes.addressInput,
                }}
                senderAddress={`${address}`}
                sendToSameAccountHelper={
                  destinationChainConfig?.type === homeConfig?.type
                }
              />
            </section>
            <div className={classes.container}>
              <Balance
                name="token"
                tokens={tokens}
                className={classes.fees}
                sync={(tokenAddress) => {
                  setPreflightDetails({
                    ...preflightDetails,
                    token: tokenAddress,
                    receiver: "",
                    tokenAmount: 0,
                    tokenSymbol: "",
                  });
                }}
              />
              <FeesFormikWrapped
                amountFormikName="tokenAmount"
                className={classes.fees}
                fee={bridgeFee}
                feeSymbol={homeConfig?.nativeTokenSymbol}
                symbol={
                  preflightDetails && tokens[preflightDetails.token]
                    ? tokens[preflightDetails.token].symbol
                    : undefined
                }
              />
            </div>

            <section>
              <button
                type="submit"
                className={classes.submitButton}
                disabled={!homeConfig || !address || props.isValidating}
              >
                Start transfer
              </button>
            </section>
          </Form>
        )}
      </Formik>
      <AboutDrawer open={aboutOpen} close={() => setAboutOpen(false)} />
      <ChangeNetworkDrawer
        open={changeNetworkOpen}
        close={() => setChangeNetworkOpen(false)}
      />
      <PreflightModalTransfer
        open={preflightModalOpen}
        close={() => setPreflightModalOpen(false)}
        receiver={preflightDetails?.receiver || ""}
        sender={address || ""}
        start={() => {
          setPreflightModalOpen(false);
          preflightDetails &&
            deposit(
              preflightDetails.tokenAmount,
              preflightDetails.receiver,
              preflightDetails.token
            );
        }}
        sourceNetwork={homeConfig?.name || ""}
        targetNetwork={destinationChainConfig?.name || ""}
        tokenSymbol={preflightDetails?.tokenSymbol || ""}
        value={preflightDetails?.tokenAmount || 0}
      />
      <TransferActiveModal open={!!transactionStatus} close={resetDeposit} />
      {/* This is here due to requiring router */}
      <NetworkUnsupportedModal />
    </article>
  );
};

export default TransferPage;
