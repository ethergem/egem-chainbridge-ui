import React, { useCallback, useState } from "react";

import { makeStyles, createStyles, ITheme } from "@chainsafe/common-theme";
import { CheckboxInput, FormikTextInputProps, TextInput } from "@chainsafe/common-components";
import clsx from "clsx";
import { useField } from "formik";

const useStyles = makeStyles(({ constants }: ITheme) =>
  createStyles({
    root: {},
    input: {
      margin: 0,
      width: "100%",
      "& input": {
        background: "#E4EBFD",
        borderRadius: 8,
        padding: "10px 1em !important",
        fontFamily: "gilroy-medium",
        fontSize: "14px",
        lineHeight: "22px",
        border: "none",
        outline: "none",
        "&::placeholder": {
          fontFamily: "gilroy-medium",
          fontSize: "14px",
          lineHeight: "22px",
          letterSpacing: "0.03em",
          color: "#3E424F",
          opacity: "0.5",
        },
      },
    },
    label: {
      marginBottom: 10,
      fontFamily: "gilroy-bold",
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#1C1E24",
    },
    checkbox: {
      marginTop: constants.generalUnit * 3,
    },
    checkBoxLabel: {
      fontFamily: "gilroy-medium",
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#5D616D",
    },
  })
);

interface IAddressInput extends FormikTextInputProps {
  senderAddress: string;
  classNames?: {
    input?: string;
  };
  sendToSameAccountHelper?: boolean;
}

const AddressInput: React.FC<IAddressInput> = ({
  classNames,
  senderAddress,
  className,
  inputVariant = "default",
  type = "text",
  placeholder,
  name,
  size,
  label,
  labelClassName,
  captionMessage,
  sendToSameAccountHelper = false,
  ...rest
}: IAddressInput) => {
  const classes = useStyles();
  const [field, meta, helpers] = useField(name);

  const [stored, setStored] = useState<string | undefined>();

  const toggleReceiver = useCallback(() => {
    if (stored === undefined) {
      setStored(field.value);
      helpers.setValue(senderAddress);
    } else {
      helpers.setValue(stored);
      setStored(undefined);
    }
  }, [helpers, field, senderAddress, stored, setStored]);

  const inlineStyles = {
    fontFamily: "gilroy-medium",
    fontSize: "14px",
    lineHeight: "22px",
    letterSpacing: "0.03em",
    color: "#5D616D",
  };

  return (
    <section className={clsx(classes.root, className)}>
      <div>
        <TextInput
          {...rest}
          label={label ? label : field.name}
          inputVariant={inputVariant}
          type={type}
          size={size}
          className={clsx(classNames?.input, classes.input)}
          labelClassName={clsx(labelClassName, classes.label)}
          name={field.name}
          value={field.value}
          placeholder={placeholder}
          captionMessage={meta.error ? `${meta.error}` : captionMessage && captionMessage}
          state={meta.error ? "error" : undefined}
          onChange={helpers.setValue}
          disabled={stored !== undefined}
        />
      </div>
      {sendToSameAccountHelper && (
        <div className={classes.checkbox}>
          <CheckboxInput
            label="Send funds to myself."
            value={stored !== undefined}
            onChange={() => toggleReceiver()}
            style={inlineStyles}
            className={classes.checkBoxLabel}
          />
        </div>
      )}
    </section>
  );
};

export default AddressInput;
