import React, { useEffect, useState } from "react";
import { Tokens } from "@chainsafe/web3-context/dist/context/tokensReducer";
import { useField } from "formik";

interface BalanceProps {
  tokens: Tokens;
  name: string;
  sync?: (value: string) => void;
  className?: string;
}

const Balance: React.FC<BalanceProps> = ({ tokens, name, sync, className }) => {
  const inlineStyles = {
    primaryText: {
      fontFamily: "gilroy-medium",
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#1C1E24",
    },
    secondaryText: {
      fontFamily: "gilroy-medium",
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#5D616D",
    },
  };

  const [field, , helpers] = useField(name);
  const labelParsed = tokens[field.value]
    ? `${tokens[field.value]?.balance} ${tokens[field.value]?.symbol}`
    : null;

  const [synced, setSynced] = useState();
  useEffect(() => {
    if (sync && field.value !== synced) {
      setSynced(field.value);
      if (field.value !== "") {
        sync(field.value);
      }
    }
    // eslint-disable-next-line
  }, [field]);

  useEffect(() => {
    // If there is only one token, auto select
    if (Object.keys(tokens).length === 1 && field.value === "") {
      helpers.setValue(Object.keys(tokens)[0]);
    }
  }, [tokens, helpers, field.value]);

  return (
    <>
      {labelParsed && (
        <div className={className} style={{ marginBottom: 0 }}>
          <p style={inlineStyles.primaryText}>Balance</p>
          <p style={inlineStyles.secondaryText}>{labelParsed}</p>
        </div>
      )}
    </>
  );
};

export default Balance;
