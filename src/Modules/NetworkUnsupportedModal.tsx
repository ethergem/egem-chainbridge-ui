import React, { useEffect, useState } from "react";

import { makeStyles, createStyles, ITheme } from "@chainsafe/common-theme";
import CustomModal from "../Components/Custom/CustomModal";
import {
  Button,
  ExclamationCircleInverseSvg,
  Typography,
  useLocation,
} from "@chainsafe/common-components";
import { useNetworkManager } from "../Contexts/NetworkManagerContext";
import { ROUTE_LINKS } from "../Components/Routes";
import { useHomeBridge } from "../Contexts/HomeBridgeContext";
import { chainbridgeConfig } from "../chainbridgeConfig";

import warningImage from "../media/Icons/warning.svg";

const useStyles = makeStyles(({ constants, palette, breakpoints }: ITheme) =>
  createStyles({
    ternaryText: {
      fontFamily: "gilroy-medium",
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#5D616D",
    },
    modalText: {
      fontFamily: "gilroy-bold",
      fontSize: "20px",
      lineHeight: "32px",
      letterSpacing: "0.03em",
      color: "#1C1E24",
    },
    fixedModal: {
      position: "fixed",
      top: "50%",
      left: "50%",
      transform: "translate(-50%,-50%)",
      background: "#FFFFFF",
      padding: "40px",
      maxWidth: "530px",
      width: "100%",
      borderRadius: "16px",
      zIndex: 2100,
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
      textAlign: "center",
      [breakpoints.down("sm")]: {
        width: "90%",
      },
    },
    backdrop: {
      position: "fixed",
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      background: "rgba(0,0,0,0.5)",
      width: "100%",
      height: "100%",
      zIndex: 2050,
    },
  })
);

const NetworkUnsupportedModal = () => {
  const classes = useStyles();
  const { homeChainConfig, networkId } = useNetworkManager();
  const { getNetworkName, wrapTokenConfig, isReady } = useHomeBridge();
  const { pathname } = useLocation();

  const [open, setOpen] = useState(false);
  const [supportedNetworks, setSupportedNetworks] = useState<number[]>([]);

  useEffect(() => {
    if (pathname === ROUTE_LINKS.Transfer) {
      setOpen(!homeChainConfig && !!isReady);
      setSupportedNetworks(
        chainbridgeConfig.chains
          .filter((bc) => bc.networkId !== undefined)
          .map((bc) => Number(bc.networkId))
      );
    } else if (pathname === ROUTE_LINKS.Wrap) {
      setOpen(!wrapTokenConfig && !!isReady);
      setSupportedNetworks(
        chainbridgeConfig.chains
          .filter((bc) => bc.networkId !== undefined)
          .filter((bc) => bc.tokens.find((t) => t.isNativeWrappedToken))
          .map((bc) => Number(bc.networkId))
      );
    } else {
      setOpen(false);
      setSupportedNetworks([]);
    }
  }, [pathname, setOpen, homeChainConfig, isReady, wrapTokenConfig]);

  return (
    <>
      <div className={classes.fixedModal} style={{ visibility: open ? "visible" : "hidden" }}>
        <img src={warningImage} alt="logo" width={128} height={128} />
        <h4 className={classes.modalText} style={{ margin: "1em 0 10px" }}>
          Wrong network
        </h4>
        <p className={classes.ternaryText}>
          Looks like you are connected to a wrong network. Change the network from your Metamask
          wallet
        </p>
      </div>
      {open && <div className={classes.backdrop}></div>}
    </>
  );
};

export default NetworkUnsupportedModal;
