import React from "react";

import { makeStyles, createStyles, ITheme } from "@chainsafe/common-theme";
import { Button, ProgressBar, Typography } from "@chainsafe/common-components";
import CustomModal from "../Components/Custom/CustomModal";
import { useChainbridge } from "../Contexts/ChainbridgeContext";
import { EvmBridgeConfig, TokenConfig } from "../chainbridgeConfig";

const useStyles = makeStyles(({ animation, constants, palette, typography }: ITheme) =>
  createStyles({
    root: {
      width: "100%",
      fontFamily: "gilroy-medium",
    },
    inner: {
      width: "100% !important",
      maxWidth: "unset !important",
      display: "flex",
      flexDirection: "row",
      padding: `${constants.generalUnit * 5}px ${constants.generalUnit * 3.5}px`,
      bottom: 0,
      top: "unset",
      transform: "unset",
      left: 0,
      border: "none",
      borderRadius: 0,
      transitionDuration: `${animation.transform}ms`,
    },
    heading: {
      marginTop: constants.generalUnit / 2,
    },
    stepIndicator: {
      ...typography.h4,
      height: 40,
      width: 40,
      marginRight: constants.generalUnit * 2,
      borderRadius: "50%",
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
      border: `1px solid ${palette.additional["transactionModal"][2]}`,
      color: palette.additional["transactionModal"][3],
      "& svg": {
        height: 20,
        width: 20,
        display: "block",
      },
    },
    content: {
      display: "flex",
      flexDirection: "column",
    },
    buttons: {
      display: "flex",
      flexDirection: "row",
      marginTop: constants.generalUnit * 5,
      "& > *": {
        textDecoration: "none",
        marginRight: constants.generalUnit,
      },
    },
    button: {
      letterSpacing: "0.03em",
      color: "#FFFFFF",
      background: "#8269D8",
      borderRadius: "100px",
      outline: "none",
      cursor: "pointer",
      fontFamily: "gilroy-medium",
      border: "1px solid #8269D8",
      transition: "300ms all ease-in-out",
      textDecoration: "none",
      "&:hover": {
        background: "#FFFFFF",
        color: "#8269D8",
        textDecoration: "none",
        border: "1px solid #8269D8",
      },
    },
    initCopy: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "space-between",
      "& > *:first-child": {
        marginTop: constants.generalUnit * 3.5,
        marginBottom: constants.generalUnit * 8,
      },
    },
    sendingCopy: {},
    vote: {
      display: "flex",
      flexDirection: "row",
      marginTop: constants.generalUnit,
      "& > *": {
        "&:first-child": {
          overflow: "hidden",
          textOverflow: "ellipsis",
          maxWidth: 240,
        },
        "&:last-child": {
          marginLeft: constants.generalUnit * 3.5,
          fontStyle: "italic",
        },
      },
    },
    warning: {
      marginTop: constants.generalUnit * 3.5,
      display: "block",
      fontWeight: 600,
      fontFamily: "gilroy-medium",
      letterSpacing: "0.03em",
      color: "#1C1E24",
    },
    receipt: {
      marginTop: constants.generalUnit * 3.5,
      marginBottom: constants.generalUnit * 8,
      fontFamily: "gilroy-medium",
      letterSpacing: "0.03em",
      color: "#1C1E24",
    },
    weighted: {
      fontWeight: 600,
      fontFamily: "gilroy-bold",
      letterSpacing: "0.03em",
      color: "#1C1E24",
    },
    progress: {
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      "& > *": {
        borderRadius: "0 !important",
        "&  >  *": {
          borderRadius: "0 !important",
          background: `#8269D8 !important`,
        },
      },
    },
  })
);

interface IWrapActiveModalProps {
  txState?: "inProgress" | "done";
  value: number;
  tokenInfo: TokenConfig;
  txHash?: string;
  close: () => void;
  action: "wrap" | "unwrap";
}

const WrapActiveModal: React.FC<IWrapActiveModalProps> = ({
  txState,
  value,
  tokenInfo,
  txHash,
  close,
  action,
}: IWrapActiveModalProps) => {
  const classes = useStyles();
  const { homeConfig } = useChainbridge();

  return (
    <CustomModal
      className={classes.root}
      injectedClass={{
        inner: classes.inner,
      }}
      active={!!txState}
    >
      <ProgressBar
        className={classes.progress}
        size="small"
        variant="primary"
        progress={txState !== "done" ? -1 : 100}
      />
      <section>
        <div className={classes.stepIndicator}>{txState === "inProgress" ? 1 : 2}</div>
      </section>
      <section className={classes.content}>
        <Typography className={classes.heading} variant="h3" component="h3">
          {txState === "inProgress"
            ? action === "wrap"
              ? `Wrapping ${value} ${homeConfig?.nativeTokenSymbol}`
              : `Unwrapping ${value} ${tokenInfo.symbol}`
            : action === "wrap"
            ? "Token wrapped"
            : "Token unwrapped"}
        </Typography>
        {txState !== "inProgress" && (
          <>
            <Typography className={classes.receipt} component="p">
              {action === "wrap"
                ? `Successfully wrapped ${homeConfig?.nativeTokenSymbol} to ${tokenInfo.symbol}`
                : `Successfully unwrapped ${tokenInfo.symbol} to ${homeConfig?.nativeTokenSymbol}`}
              {homeConfig && (homeConfig as EvmBridgeConfig).blockExplorer && txHash && (
                <>
                  <br />
                  <a
                    rel="noopener noreferrer"
                    target="_blank"
                    href={`${(homeConfig as EvmBridgeConfig).blockExplorer}/${txHash}`}
                  >
                    View Transaction
                  </a>
                </>
              )}
            </Typography>
            <section className={classes.buttons}>
              <Button
                size="small"
                className={classes.button}
                variant="outline"
                onClick={() => close()}
              >
                Start a transfer
              </Button>
              <Button
                size="small"
                className={classes.button}
                variant="outline"
                onClick={() => {
                  close();
                }}
              >
                Close Window
              </Button>
            </section>
          </>
        )}
      </section>
    </CustomModal>
  );
};

export default WrapActiveModal;
