import React from "react";

import { makeStyles, createStyles, ITheme } from "@chainsafe/common-theme";
import CustomDrawer from "../Components/Custom/CustomDrawer";
import { Button, Typography } from "@chainsafe/common-components";
import { shortenAddress } from "../Utils/Helpers";

const useStyles = makeStyles(({ constants, palette, zIndex }: ITheme) =>
  createStyles({
    root: {
      zIndex: zIndex?.blocker,
      position: "absolute",
      fontFamily: "gilroy-medium",
      overflowY: "auto",
      width: "100% !important",
      "&::-webkit-scrollbar": {
        width: 5,
        background: "#fff",
      },
      "&::-webkit-scrollbar-track": {
        boxShadow: "inset 0 0 6px rgba(0, 0, 0, 0.3)",
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "#8269D8",
      },
      "& li": {
        position: "relative",
        padding: `${constants.generalUnit}px 0 ${constants.generalUnit}px ${
          constants.generalUnit * 8
        }px`,
        "&:before": {
          content: "''",
          display: "block",
          backgroundColor: "#000",
          height: constants.generalUnit,
          width: constants.generalUnit,
          borderRadius: "50%",
          position: "absolute",
          top: "50%",
          left: constants.generalUnit * 4,
          transform: "translate(-50%, -50%)",
        },
      },
    },
    title: {
      fontFamily: "gilroy-bold",
    },
    subtitle: {
      margin: `${constants.generalUnit * 2}px 0`,
      fontFamily: "gilroy-medium",
    },
    agreement: {
      margin: `${constants.generalUnit * 2}px 0`,
      fontFamily: "gilroy-medium",
    },
    startButton: {
      backgroundColor: "#8269D8",
      color: "#FFFFFF",
      marginBottom: constants.generalUnit * 2,
      outline: "none",
      border: "1px solid #8269D8",
      "&:hover": {
        background: "#FFFFFF",
        color: "#8269D8",
        border: "1px solid #8269D8",
      },
    },
    backdrop: {
      position: "absolute",
      zIndex: zIndex?.layer4,
    },
    liText: {
      fontFamily: "gilroy-medium",
    },
  })
);

interface IPreflightModalTransferProps {
  open: boolean;
  close: () => void;
  sender: string;
  receiver: string;
  value: number;
  tokenSymbol: string;
  sourceNetwork: string;
  targetNetwork: string;
  start: () => void;
}

const PreflightModalTransfer: React.FC<IPreflightModalTransferProps> = ({
  open,
  close,
  receiver,
  sender,
  sourceNetwork,
  targetNetwork,
  tokenSymbol,
  value,
  start,
}: IPreflightModalTransferProps) => {
  const classes = useStyles();

  return (
    <CustomDrawer
      className={classes.root}
      classNames={{
        backdrop: classes.backdrop,
      }}
      size={430}
      open={open}
    >
      <Typography variant="h3" component="h2" className={classes.title}>
        Pre-flight check
      </Typography>
      <Typography className={classes.subtitle} variant="h5" component="p">
        Please be advised this is an experimental application:
      </Typography>
      <ul>
        <li>
          <Typography variant="h5" className={classes.liText}>
            You will not be able to cancel the transaction once you submit it.
          </Typography>
        </li>
        <li>
          <Typography variant="h5" className={classes.liText}>
            Your transaction could get stuck for an indefinite amount of time
          </Typography>
        </li>
        <li>
          <Typography variant="h5" className={classes.liText}>
            Funds cannot be returned if they are sent to the wrong address.
          </Typography>
        </li>
        <li>
          <Typography variant="h5" className={classes.liText}>
            The transaction fee may be higher than expected.
          </Typography>
        </li>
      </ul>
      <Typography className={classes.agreement} variant="h5" component="p">
        I agree and want to send{" "}
        <strong>
          {value} {tokenSymbol}
        </strong>{" "}
        from&nbsp;
        <strong>{shortenAddress(sender)}</strong> on <strong>{sourceNetwork}</strong> to&nbsp;
        <strong>{shortenAddress(receiver)}</strong> on <strong>{targetNetwork}</strong>.
      </Typography>
      <Button onClick={start} className={classes.startButton} fullsize>
        Start Transfer
      </Button>
      <Button onClick={close}>Back</Button>
    </CustomDrawer>
  );
};

export default PreflightModalTransfer;
