import React from "react";
import { createStyles, ITheme, makeStyles } from "@chainsafe/common-theme";
import { useChainbridge } from "../Contexts/ChainbridgeContext";
import { useNetworkManager } from "../Contexts/NetworkManagerContext";
import { Button, Modal, ProgressBar, Typography } from "@chainsafe/common-components";

import metamasklogo from "../media/Icons/metamask.png";

const useStyles = makeStyles(({ constants, palette, zIndex }: ITheme) => {
  return createStyles({
    root: {},
    slide: {
      borderRadius: constants.generalUnit / 2,
      padding: `${constants.generalUnit}px ${constants.generalUnit * 2}px`,
      "& > p": {
        marginTop: constants.generalUnit * 2,
        marginBottom: constants.generalUnit * 3,
        textAlign: "center",
      },
    },
    buttons: {
      marginBottom: constants.generalUnit * 2,
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-around",
    },
    primaryBtn: {
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#FFFFFF",
      background: "#8269D8",
      borderRadius: "100px",
      padding: "6px 24px",
      outline: "none",
      cursor: "pointer",
      fontFamily: "gilroy-medium",
      border: "1px solid #8269D8",
      transition: "300ms all ease-in-out",
      "&:hover": {
        background: "#FFFFFF",
        color: "#8269D8",
        border: "1px solid #8269D8",
      },
    },
    fixedModal: {
      padding: "40px 0",
      // maxWidth: "530px",
      // width: "100%",
      // borderRadius: "16px",
      // zIndex: 2100,
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      flexDirection: "column",
      textAlign: "center",
    },
    modalText: {
      fontFamily: "gilroy-bold",
      fontSize: "20px",
      lineHeight: "32px",
      letterSpacing: "0.03em",
      color: "#1C1E24",
    },
    ternaryText: {
      fontFamily: "gilroy-medium",
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#5D616D",
    },
  });
});

const NetworkSelectModal = () => {
  const classes = useStyles();
  const { isReady, chains } = useChainbridge();
  const { walletType, setWalletType } = useNetworkManager();

  return (
    <Modal
      active={walletType !== "unset" && walletType !== "Ethereum" && !isReady}
      closePosition="right"
      className={classes.root}
      injectedClass={{
        inner: classes.slide,
      }}
    >
      {walletType === "select" && (
        <>
          <div className={classes.fixedModal}>
            <img src={metamasklogo} alt="logo" width={128} height={128} />
            <h4 className={classes.modalText} style={{ margin: "1em 0 3px" }}>
              Connect to your metamask wallet
            </h4>
            <button
              className={classes.primaryBtn}
              style={{ marginTop: "1.25em" }}
              onClick={() => setWalletType("Ethereum")}
            >
              Connect wallet
            </button>
          </div>
        </>
      )}
      {walletType === "Substrate" && (
        <>
          <Typography variant="h2" component="p">
            Connecting to node
          </Typography>
          <ProgressBar size="small" variant="primary" />
        </>
      )}
    </Modal>
  );
};

export default NetworkSelectModal;
