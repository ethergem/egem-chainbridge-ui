import { NavLink, Typography, useHistory, useLocation } from "@chainsafe/common-components";
import { createStyles, ITheme, makeStyles } from "@chainsafe/common-theme";
import React, { useEffect, useState } from "react";
import { ReactNode } from "react";
import AppHeader from "./AppHeader";
import { ReactComponent as TransferSvg } from "../media/Icons/transfer.svg";
import { ReactComponent as GiftSvg } from "../media/Icons/gift.svg";
import { ROUTE_LINKS } from "../Components/Routes";

interface IAppWrapper {
  children: ReactNode | ReactNode[];
  wrapTokenPage?: boolean;
}

const useStyles = makeStyles(({ animation, constants, palette, breakpoints }: ITheme) => {
  return createStyles({
    root: {
      minHeight: "100vh",
      display: "flex",
      flexDirection: "column",
      paddingTop: 0,
      marginTop: 0,
      paddingLeft: 10,
      paddingRight: 10,
      [breakpoints.between("sm", "xl")]: {
        paddingTop: 0,
        marginTop: 0,
        paddingLeft: 70,
        paddingRight: 70,
      },
    },
    inner: {
      paddingTop: (constants.navItemHeight as number) * 2,
      paddingBottom: (constants.navItemHeight as number) * 2,
    },
    cta: {
      display: "block",
      maxWidth: 200,
      maxHeight: 200,
      position: "fixed",
      bottom: constants.generalUnit * 3,
      right: constants.generalUnit * 3,
    },
    content: {
      // position: "absolute",
      // top: "50%",
      // left: "50%",
      // transform: "translate(-50%, -50%)",
      margin: `20px auto`,
      maxWidth: 460,
      width: "100%",
      display: "flex",
      flexDirection: "column",
      overflow: "hidden",
      borderRadius: 4,
      [breakpoints.between("sm", "xl")]: {
        margin: `20px 0`,
      },
    },
    explorerMainContent: {
      width: "100%",
      height: "100%",
      margin: "0 auto",
    },
    pageArea: {
      height: "100%",
      width: "100%",
      overflow: "hidden",
    },
    explorerArea: {
      width: "100%",
      height: "100vh",
      marginTop: 86,
    },
    navTabs: {
      width: "100%",
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "flex-start",
      padding: `0 ${constants.generalUnit}px`,
      transform: "translateY(1px)",
      "& > a": {
        display: "flex",
        flexDirection: "row",
        alignItems: "center",
        padding: `${constants.generalUnit}px ${constants.generalUnit * 1.5}px`,
        border: `1px solid ${palette.additional["gray"][7]}`,
        textDecoration: "none",
        marginRight: constants.generalUnit,
        transitionDuration: `${animation.transform}ms`,
        color: palette.additional["gray"][8],
        maxHeight: constants.navItemHeight,
        "& svg": {
          transitionDuration: `${animation.transform}ms`,
          fill: palette.additional["gray"][8],
        },
        "&.active": {
          color: palette.additional["gray"][9],
          textDecoration: "underline",
          "& svg": {
            fill: palette.additional["geekblue"][5],
          },
        },
        "& > *:first-child": {
          marginRight: constants.generalUnit,
        },
      },
      "& svg": {
        height: 14,
        width: 14,
      },
    },
    navHeader: {
      display: "flex",
      alignItems: "center",
      marginBottom: "30px",
      "& svg": {
        height: 32,
        width: 32,
        marginRight: 10,
      },
      "& p": {
        fontFamily: "gilroy-bold",
        fontSize: "28px",
        lineHight: "36px",
        letterSpacing: "0.03em",
        color: "#FFFFFF",
      },
    },
  });
});

const AppWrapper: React.FC<IAppWrapper> = ({ children, wrapTokenPage }: IAppWrapper) => {
  const classes = useStyles();
  const [enableNavTabs, setEnableNavTabs] = useState(true);

  const location = useLocation();
  const { redirect } = useHistory();

  const { __RUNTIME_CONFIG__ } = window;

  const indexerEnabled = "INDEXER_URL" in __RUNTIME_CONFIG__;

  useEffect(() => {
    if (location.pathname.includes("/explorer/list") && !indexerEnabled) {
      redirect("/transfer");
    }
  }, []);

  useEffect(() => {
    if (location.pathname.includes("/explorer/list")) {
      return setEnableNavTabs(false);
    }
    return setEnableNavTabs(true);
  }, [location]);

  return (
    <section className={classes.root}>
      {enableNavTabs ? (
        <section>
          {/* <section className={classes.inner}> */}
          <AppHeader />
          <section className={classes.content}>
            {enableNavTabs && (
              <section className={classes.navHeader}>
                <TransferSvg />
                <p>Transfer</p>
                {wrapTokenPage && (
                  <NavLink activeClassName="active" to={ROUTE_LINKS.Wrap}>
                    <GiftSvg />
                    <Typography variant="h5">Wrap token</Typography>
                  </NavLink>
                )}
              </section>
            )}
            <div className={classes.pageArea}>{children}</div>
          </section>

          {/* Put CTA here */}
          {/* <a className={classes.cta} rel="noopener noreferrer" target="_blank" href="#">
        </a> */}
        </section>
      ) : (
        <section className={classes.explorerMainContent}>
          <AppHeader />
          <div className={classes.explorerArea}>{children}</div>
        </section>
      )}
    </section>
  );
};

export default AppWrapper;
