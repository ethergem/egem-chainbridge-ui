import { createStyles, ITheme, makeStyles } from "@chainsafe/common-theme";
import React from "react";
import clsx from "clsx";
import { Typography, NavLink } from "@chainsafe/common-components";
import { shortenAddress } from "../Utils/Helpers";
import { useChainbridge } from "../Contexts/ChainbridgeContext";
import { useNetworkManager } from "../Contexts/NetworkManagerContext";

import logo from "../media/Icons/logo.png";

const ROUTE_LINKS_HEADERS = [
  { route: "/transfer", label: "Transfer" },
  { route: "/explorer/list", label: "Explorer" },
];

const useStyles = makeStyles(({ constants, palette, zIndex, breakpoints }: ITheme) => {
  return createStyles({
    root: {
      display: "flex",
      // position: "fixed",
      justifyContent: "space-between",
      flexWrap: "wrap",
      padding: `10px 0px`,
      width: "100%",
      top: 0,
      left: 0,
      backgroundColor: "transparent",
      color: palette.additional["header"][2],
      alignItems: "center",
      zIndex: zIndex?.layer2,
      [breakpoints.down("sm")]: {
        flexDirection: "column",
        padding: "10px 30px",
        gridGap: "1em",
      },
    },
    left: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "flex-start",
      alignItems: "center",
      [breakpoints.down("sm")]: {
        display: "flex",
        flexDirection: "column",
      },
    },
    logo: {
      height: constants.generalUnit * 5,
      width: constants.generalUnit * 5,
      "& svg, & img": {
        maxHeight: "100%",
        maxWidth: "100%",
      },
    },
    state: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
    },
    indicator: {
      display: "block",
      height: 8,
      width: 8,
      borderRadius: "50%",
      backgroundColor: "#A3E949",
      marginRight: constants.generalUnit,
    },
    address: {
      marginRight: constants.generalUnit,
    },
    network: {},
    accountInfo: {
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
    },
    mainInfo: {
      display: "flex",
      flexDirection: "row",
      gridGap: "1em",
      flexWrap: "wrap",
      alignItems: "center",
      justifyContent: "center",
    },
    mainTitle: {},
    headerLinks: {
      marginLeft: 49,
      [breakpoints.down("sm")]: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        marginLeft: "unset",
        alignItems: "center",
        width: "100%",
        "& > a:last-child": {
          marginLeft: 5,
        },
      },
    },
    link: {
      marginLeft: 46,
      textDecoration: "none",
      [breakpoints.down("sm")]: {
        marginLeft: "unset",
      },
    },
    linkTitle: {
      fontSize: 16,
    },
    primaryBtn: {
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#1C1E24",
      background: "#FFFFFF",
      borderRadius: "100px",
      padding: "6px 24px",
      outline: "none",
      border: "none",
      cursor: "pointer",
      fontFamily: "gilroy-medium",
      transition: "300ms all ease-in-out",
      "&:hover": {
        background: "#8987DF",
        color: "#FFFFFF",
      },
    },
    text: {
      fontSize: "14px",
      lineHeight: "22px",
      letterSpacing: "0.03em",
      color: "#FFFFFF",
    },
    headerLogo: {
      // width: "240px",
      height: "36px",
    },
  });
});

interface IAppHeader {}

const AppHeader: React.FC<IAppHeader> = () => {
  const classes = useStyles();
  const { homeConfig, isReady, address } = useChainbridge();
  const { walletType, setWalletType } = useNetworkManager();

  const { __RUNTIME_CONFIG__ } = window;

  const indexerEnabled = "INDEXER_URL" in __RUNTIME_CONFIG__;

  return (
    <header className={clsx(classes.root)}>
      <div className={classes.left}>
        {/* ADD LOGO HERE */}
        {/* <div className={classes.logo}>
        </div> */}
        <div className={classes.mainTitle}>
          <img src={logo} alt="logo" className={classes.headerLogo} />
        </div>
        {/* <div className={classes.headerLinks}>
          {indexerEnabled ? (
            ROUTE_LINKS_HEADERS.map(({ route, label }) => (
              <NavLink to={route} className={classes.link} key={route}>
                <Typography className={classes.linkTitle}>{label}</Typography>
              </NavLink>
            ))
          ) : (
            <NavLink
              to={ROUTE_LINKS_HEADERS[0].route}
              className={classes.link}
              key={ROUTE_LINKS_HEADERS[0].route}
            >
              <Typography className={classes.linkTitle}>
                {ROUTE_LINKS_HEADERS[0].label}
              </Typography>
            </NavLink>
          )}
        </div> */}
      </div>
      <section className={classes.state}>
        {!isReady ? (
          <button
            className={classes.primaryBtn}
            onClick={() => {
              setWalletType("select");
            }}
          >
            Connect Wallet
          </button>
        ) : (
          <>
            <div className={classes.mainInfo}>
              <div className={classes.accountInfo}>
                <span className={classes.indicator} />
                <span className={classes.text}>Connected to {homeConfig?.name} </span>
              </div>
              <button className={classes.primaryBtn}>{address && shortenAddress(address)}</button>
            </div>
            {/* <div className={classes.mainInfo}>
              <div className={classes.accountInfo}>
                <Typography variant="h5" className={classes.address}>
                  {address && shortenAddress(address)}
                </Typography>
              </div>
              <Typography variant="h5" className={classes.address}>
                <div>
                  <span>Connected to </span>
                  <span>
                    <strong>{homeConfig?.name}</strong>
                  </span>
                </div>
              </Typography>
            </div> */}
          </>
        )}
      </section>
    </header>
  );
};

export default AppHeader;
