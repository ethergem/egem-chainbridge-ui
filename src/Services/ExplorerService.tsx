import {
  DepositRecord,
  ExplorerState,
} from "../Contexts/Reducers/TransfersReducer";

export const fetchTransfers = async (
  setState: React.SetStateAction<any>,
  state: ExplorerState
): Promise<any> => {
  const {
    __RUNTIME_CONFIG__: { INDEXER_URL },
  } = window;
  try {
    const response = await fetch(`${INDEXER_URL}/transfers`);
    const responseParsed = await response.json();
    const transfers: Array<DepositRecord> = responseParsed.transfers;
    setState({
      ...state,
      transfers,
    });
  } catch (error) {
    console.log("ERROR", error);
    setState({
      ...state,
      error: true,
    });
  }
};
