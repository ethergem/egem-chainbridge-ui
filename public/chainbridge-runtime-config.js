window.__RUNTIME_CONFIG__ = {
  INDEXER__URL: "http://localhost:8000",
  CHAINBRIDGE: {
    chains: [
      {
        chainId: 195,
        networkId: 1987,
        name: "EtherGem (EGEM)",
        decimals: 18,
        bridgeAddress: "0xf2Db01B927C712baE5829BFD1B7E694b16F91fe1",
        erc20HandlerAddress: "0x4d6eEaC6912608529114Cc72b3fB6C27Fd812942",
        rpcUrl: "https://lb.rpc.egem.io",
        type: "Ethereum",
        nativeTokenSymbol: "EGEM",
        tokens: [
          {
            address: "0x95e3D9C12C1a8bF3c80782283900FfE77fB8c295",
            name: "Wrapped MATIC",
            symbol: "WMATIC",
            imageUri: "WMATICIcon",
            resourceId:
              "0x137000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce00",
          },
          {
            address: "0xE5fca20e55811D461800A853f444FBC6f5B72BEa",
            name: "WrappedEGEM",
            symbol: "WEGEM",
            imageUri: "WEGEMIcon",
            resourceId:
              "0x195000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce00",
          },
          {
            address: "0xC99A0e60fB489Ab229140D0F5751916858Ab62C8",
            name: "Wrapped Ether",
            symbol: "WETH",
            imageUri: "WETHIcon",
            resourceId:
              "0x137000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce02",
          },
          {
            address: "0xB6094af67bf43779ab704455c5DF02AD9141871B",
            name: "RUBY",
            symbol: "RUBY",
            imageUri: "WRUBYIcon",
            resourceId:
              "0x195000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce01",
          },
          {
            address: "0x33F4999ee298CAa16265E87f00e7A8671c01D870",
            name: "TrueUSD",
            symbol: "TUSD",
            imageUri: "TUSDIcon",
            resourceId:
              "0x560000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce01",
          },
          {
            address: "0x97271a78C9778F226C1c4eFe63876c04f8cD8F40",
            name: "WrappedBNB",
            symbol: "WBNB",
            imageUri: "WBNBIcon",
            resourceId:
              "0x560000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce02",
          },
          {
            address: "0x7Fd4B3e33985bb93abC32A8Aa0839dAd0Ac1CB04",
            name: "datber Capital",
            symbol: "BEIR",
            imageUri: "BEIRIcon",
            resourceId:
              "0x160000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce01",
          },
          {
            address: "0x6ed056d4724770062406CE6F934ec173e9F5eb51",
            name: "BitCone",
            symbol: "CONE",
            imageUri: "CONEICon",
            resourceId:
              "0x106000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce02",
          },
        ],
      },
      {
        chainId: 56,
        networkId: 56,
        name: "Binance Smart Chain (BNB)",
        decimals: 18,
        bridgeAddress: "0x5a4A110cF45D100FE0eA13b4aec043bBf4FFD0ff",
        erc20HandlerAddress: "0x531f6097E247FF6Ea92635fdB0Db8981aCD2f8D9",
        rpcUrl: "https://bsc-dataseed.binance.org/",
        type: "Ethereum",
        nativeTokenSymbol: "BNB",
        tokens: [
          {
            address: "0xF6AD9a164a0f1CfE5C3e7bc5c2B9E684de4aC9c5",
            name: "WrappedEGEM",
            symbol: "WEGEM",
            imageUri: "WEGEMIcon",
            resourceId:
              "0x195000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce00",
          },
          {
            address: "0x4d6eEaC6912608529114Cc72b3fB6C27Fd812942",
            name: "WrappedRUBY",
            symbol: "WRUBY",
            imageUri: "WRUBYIcon",
            resourceId:
              "0x195000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce01",
          },
          {
            address: "0x14016e85a25aeb13065688cafb43044c2ef86784",
            name: "TrueUSD",
            symbol: "TUSD",
            imageUri: "TUSDIcon",
            resourceId:
              "0x560000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce01",
          },
          {
            address: "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c",
            name: "WrappedBNB",
            symbol: "WBNB",
            imageUri: "WBNBIcon",
            resourceId:
              "0x560000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce02",
          },
        ],
      },
      {
        chainId: 16,
        networkId: 10000,
        name: "Smart BCH (BCH)",
        decimals: 18,
        bridgeAddress: "0x5a4A110cF45D100FE0eA13b4aec043bBf4FFD0ff",
        erc20HandlerAddress: "0x531f6097E247FF6Ea92635fdB0Db8981aCD2f8D9",
        rpcUrl: "https://global.uat.cash/",
        type: "Ethereum",
        nativeTokenSymbol: "BCH",
        tokens: [
          {
            address: "0xF6AD9a164a0f1CfE5C3e7bc5c2B9E684de4aC9c5",
            name: "WrappedEGEM",
            symbol: "WEGEM",
            imageUri: "WEGEMIcon",
            resourceId:
              "0x195000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce00",
          },
          {
            address: "0x686C2bb8B9aa391E340E1362A89daB55520504C9",
            name: "datber Capital",
            symbol: "BEIR",
            imageUri: "BEIRIcon",
            resourceId:
              "0x160000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce01",
          },
        ],
      },
      {
        chainId: 106,
        networkId: 43114,
        name: "Avalanche C-Chain (AVAX)",
        decimals: 18,
        bridgeAddress: "0x5a4A110cF45D100FE0eA13b4aec043bBf4FFD0ff",
        erc20HandlerAddress: "0x531f6097E247FF6Ea92635fdB0Db8981aCD2f8D9",
        rpcUrl: "https://api.avax.network/ext/bc/C/rpc",
        type: "Ethereum",
        nativeTokenSymbol: "AVAX",
        tokens: [
          {
            address: "0xF6AD9a164a0f1CfE5C3e7bc5c2B9E684de4aC9c5",
            name: "WrappedEGEM",
            symbol: "WEGEM",
            imageUri: "WEGEMIcon",
            resourceId:
              "0x195000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce00",
          },
        ],
      },
      {
        chainId: 137,
        networkId: 137,
        name: "Polygon (Matic)",
        decimals: 18,
        bridgeAddress: "0x5a4A110cF45D100FE0eA13b4aec043bBf4FFD0ff",
        erc20HandlerAddress: "0x531f6097E247FF6Ea92635fdB0Db8981aCD2f8D9",
        rpcUrl: "https://polygon-rpc.com",
        type: "Ethereum",
        nativeTokenSymbol: "MATIC",
        tokens: [
          {
            address: "0x0d500B1d8E8eF31E21C99d1Db9A6444d3ADf1270",
            name: "Wrapped MATIC",
            symbol: "WMATIC",
            imageUri: "WMATICIcon",
            resourceId:
              "0x137000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce00",
          },
          {
            address: "0xF6AD9a164a0f1CfE5C3e7bc5c2B9E684de4aC9c5",
            name: "WrappedEGEM",
            symbol: "WEGEM",
            imageUri: "WEGEMIcon",
            resourceId:
              "0x195000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce00",
          },
          {
            address: "0x9137E1C9Df6914264be89429983916b2FA253596",
            name: "datber Capital",
            symbol: "BEIR",
            imageUri: "BEIRIcon",
            resourceId:
              "0x160000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce01",
          },
          {
            address: "0xbA777aE3a3C91fCD83EF85bfe65410592Bdd0f7c",
            name: "BitCone",
            symbol: "CONE",
            imageUri: "CONEICon",
            resourceId:
              "0x106000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce02",
          },
          {
            address: "0x7ceB23fD6bC0adD59E62ac25578270cFf1b9f619",
            name: "Wrapped Ether",
            symbol: "WETH",
            imageUri: "WETHIcon",
            resourceId:
              "0x137000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce02",
          },
        ],
      },
    ],
  },
};
